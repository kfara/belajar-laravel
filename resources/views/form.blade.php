@extends('layout.master')

@section('title')
Form Pendaftaran
@endsection

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas 1 Pekan 1</title>
</head>
<body>
@section('content')
    <h3>Create New Account</h3>
    <h5>Sign Up Form</h5>
    <form action="/welcomes" method="post">
        @csrf
    <label>First Name :</label> <br>
    <input type="text" name="Fname"> <br> <br>
    <label>Last Name :</label> <br>
    <input type="text" name="Lname"> <br> <br>
    <label>Gender</label> <br> <br>
    <input type="radio" name="Gender" value="Male"> Male <br>
    <input type="radio" name="Gender" value="Female"> Female <br> <br>
    <label>Natonality</label> <br> <br>
    <select name="Nationality">
        <option value="1">Indonesia</option>
        <option value="2">American</option>
        <option value="3">Chinese</option>
    </select> <br> <br>
    <label>Language Spoken</label> <br> <br>
    <input type="checkbox" value="1" name="Language"> Bahasa Indonesia <br>
    <input type="checkbox" value="2" name="Language"> English <br>
    <input type="checkbox" value="3" name="Language"> Other <br> <br>
    <label>Bio</label> <br> <br>
    <textarea name="Bio" cols="30" rows="10"></textarea> <br> <br>
    <button>Sign Up</button>
    </form>
@endsection
</body>
</html>