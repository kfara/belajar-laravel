@extends('layout.master')

@section('title')
Halaman Home
@endsection

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas 1 Pekan 1</title>
</head>
<body>
@section('content')
    <h1>Selamat Datang {{$NamaDepan}} {{$NamaBelakang}}!</h1>
    <h3>Terima kasih telah bergabung di Website kami, Media Belajar kita bersama</h3>
@endsection
</body>
</html>