<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('form');
    }
    public function welcomes(Request $request)
    {
        // dd($request->all());
        $NamaDepan = $request['Fname'];
        $NamaBelakang = $request['Lname'];

        return view('welcomes', compact('NamaDepan' , 'NamaBelakang'));
    }
}
